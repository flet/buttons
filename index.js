
const path = require('path')
var player = require('play-sound')()

const sounds = [
  path.join(__dirname, 'sounds/failed.mp3'),
  path.join(__dirname, 'sounds/applause.mp3'),
  path.join(__dirname, 'sounds/crickets.mp3'),
  path.join(__dirname, 'sounds/rimshot.mp3')
]

// $ mplayer foo.mp3
player.play(path.join(__dirname, 'sounds/people_saying_oohhh.mp3'), function (err) {
  if (err) throw err
})

var gamepad = require('gamepad')

// Initialize the library
gamepad.init()
// List the state of all currently attached devices
for (var i = 0, l = gamepad.numDevices(); i < l; i++) {
  console.log(i, gamepad.deviceAtIndex())
}

// Create a game loop and poll for events
setInterval(gamepad.processEvents, 16)
// Scan for new gamepads as a slower rate
setInterval(gamepad.detectDevices, 500)

// Listen for button down events on all gamepads
gamepad.on('down', function (id, num) {
  player.play(sounds[num])
})
